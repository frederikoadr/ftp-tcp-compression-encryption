using System;
using System.Text;
using System.Security.Cryptography;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Security.AccessControl;

namespace EncryptStringSample
{
    public static class StringCipher
    {
        public static byte[] Encrypting(byte[] input, string pass) //buat aes encrypt
        {
            PasswordDeriveBytes pdb =
              new PasswordDeriveBytes(pass, 
              new byte[] { 0x43, 0x87, 0x23, 0x72 });
            MemoryStream ms = new MemoryStream();
            Aes aes = new AesManaged();
            aes.Key = pdb.GetBytes(aes.KeySize / 8);
            aes.IV = pdb.GetBytes(aes.BlockSize / 8);
            CryptoStream cs = new CryptoStream(ms,
              aes.CreateEncryptor(), CryptoStreamMode.Write);
            cs.Write(input, 0, input.Length);
            cs.Close();
            return ms.ToArray();
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                TcpClient tcpClient = new TcpClient("localhost", 8080);
                Console.WriteLine("Connected");

                NetworkStream nstream = tcpClient.GetStream();
                StreamWriter sWriter = new StreamWriter(nstream);
                BinaryWriter bWriter = new BinaryWriter(nstream); //instead pake networkstream error

                Console.WriteLine("Please enter a public key to use:");
                string password = Console.ReadLine();
                Console.WriteLine("Please enter a file path to compress and encrypt:");
                string filePath = Console.ReadLine();
                
                string zipPath = Environment.CurrentDirectory + @"\lol.zip";
                ZipFile.CreateFromDirectory(filePath, zipPath); //buat compress
                Console.WriteLine("Zipped!");

                byte[] bytes = File.ReadAllBytes(zipPath);

                sWriter.WriteLine(password);
                sWriter.Flush();
                byte[] encryptedbyte = StringCipher.Encrypting(bytes, password); //buat encrypt
                int pnjgbyte = encryptedbyte.Length;
                sWriter.WriteLine(encryptedbyte.Length.ToString()); //ngirim panjang byte enkripsi(Penting!)
                sWriter.Flush();
                //*nStream.Write(encryptedbyte, 0, pnjgbyte) -> (instead using this) pakai ini gabisa
                bWriter.Write(encryptedbyte); //* (i use this)
                sWriter.Flush();
                Console.WriteLine("Encryption Bytes Sent! >:[");

                File.Delete(zipPath);

                Console.WriteLine("");

                Console.WriteLine("Press any key to exit...");
                Console.ReadLine();
            }
            catch (Exception e)
            {
                Console.Write(e.Message);
            }

            Console.Read();
        }
    }
}
