using System;
using System.Collections;
using System.Net;
using System.Net.Security;
using System.Net.Sockets;
using System.Security.Authentication;
using System.Text;
using System.Security.Cryptography;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.IO;
using System.IO.Compression;

namespace Examples.System.Net
{
    public static class StringCipher
    {
        public static byte[] Decrypting(byte[] input, string pass) //buat aes decrypt
        {
            PasswordDeriveBytes pdb =
              new PasswordDeriveBytes(pass,
              new byte[] { 0x43, 0x87, 0x23, 0x72 });
            MemoryStream ms = new MemoryStream();
            Aes aes = new AesManaged();
            aes.Key = pdb.GetBytes(aes.KeySize / 8);
            aes.IV = pdb.GetBytes(aes.BlockSize / 8);
            CryptoStream cs = new CryptoStream(ms,
              aes.CreateDecryptor(), CryptoStreamMode.Write);
            cs.Write(input, 0, input.Length);
            cs.Close();
            return ms.ToArray();
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            TcpListener tcpListener = new TcpListener(IPAddress.Any, 8080);
            tcpListener.Start();

            Console.WriteLine("Server started");

            while (true)
            {
                TcpClient tcpClient = tcpListener.AcceptTcpClient();

                Console.WriteLine("Connected to client");

                NetworkStream nstream = tcpClient.GetStream();
                StreamReader reader = new StreamReader(nstream);
                BinaryReader bWriter = new BinaryReader(nstream); //instead networkstream error

                string password = reader.ReadLine();
                string cmdFileSize = reader.ReadLine(); //menerima besar byte dlm string (Penting!)
                int length = Convert.ToInt32(cmdFileSize); //besar byte ke Int
                Console.WriteLine("File size : " + cmdFileSize + " bytes");

                byte[] bytes = bWriter.ReadBytes(length); //buat nerima bytenya dengan panjang 'length'
                //var read = nstream.Read(bytes, 0, length) -> pakai ini selalu error di byte length saat decrypt ntah kenapa
                Console.WriteLine("Encrypted Bytes : ");
                Console.WriteLine(bytes);
                Console.WriteLine("The public key is : ");
                Console.WriteLine(password);
                Console.WriteLine("");

                Console.WriteLine("Decrypting Bytes : ");
                byte[] decryptedbytes = StringCipher.Decrypting(bytes, password); //buat decrypt
                Console.WriteLine(decryptedbytes);

                Console.WriteLine("Put your received zip path : ");
                Console.WriteLine("(Include yourfilename.zip to your path!)");
                Console.WriteLine("(Input 'f' if you wanna save and extract to CurrentDirectory)");
                string zipPath = Console.ReadLine();
                if (zipPath == "f")
                {
                    File.WriteAllBytes(Environment.CurrentDirectory + @"\yourzipdude.zip", decryptedbytes);
                    ZipFile.ExtractToDirectory(Environment.CurrentDirectory + @"\yourzipdude.zip", Environment.CurrentDirectory);
                    Console.WriteLine("Wanna delete ur zip? (y/n) :");
                    string del = Console.ReadLine();
                    if (del == "y")
                    {
                        File.Delete(Environment.CurrentDirectory + @"\yourzipdude.zip");
                        Console.WriteLine("Zip deleted!");
                    }
                    Console.WriteLine("File received and saved in " + Environment.CurrentDirectory);
                }
                else
                {
                    File.WriteAllBytes(zipPath, decryptedbytes);
                    Console.WriteLine("Wanna extract where? : ");
                    string extractPath = Console.ReadLine();
                    ZipFile.ExtractToDirectory(zipPath, extractPath);
                    Console.WriteLine("Wanna delete ur zip? (y/n) :");
                    string del = Console.ReadLine();
                    if (del == "y")
                    {
                        File.Delete(zipPath);
                        Console.WriteLine("Zip deleted!");
                    }
                    else
                    {
                        Console.WriteLine("File received and saved in " + zipPath);
                    }
                }
                Console.WriteLine("Press any key to exit...");
                Console.ReadLine();
            }
            
        }
    }
}